export default [
  {
    name: 'Шаденко Григорий Андреевич',
    birthDay: '14061946',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Леонов Леонид Сергеевич',
    birthDay: '22071963',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Чигвинцев Владимир Владимирович',
    birthDay: '25121951',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Аксенов Олег Владимирович',
    birthDay: '01091947',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Шамин Александр Викторович',
    birthDay: '02111950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Марахотин Леонид Николаевич',
    birthDay: '13051953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Губаев Динар Файисламович',
    birthDay: '16091945',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Кайгородов Василий Иванович',
    birthDay: '19081948',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Хакимов Ренат Габдулхакович',
    birthDay: '18051952',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Вахрушев Евгений Петрович',
    birthDay: '19031947',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Волосатов Владимир Ильич',
    birthDay: '22061944',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Коковихин Валерий Семенович',
    birthDay: '11011950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Кротов Олег Алексеевич',
    birthDay: '21091962',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Мехонцев Василий Павлович',
    birthDay: '05051963',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Шерстобитов Валерий Николаевич',
    birthDay: '31071956',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Зацепин Александр Георгиевич',
    birthDay: '12011952',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Бородин Леонид Николаевич',
    birthDay: '17101951',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Корнилов Сергей Владимирович',
    birthDay: '21031962',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Селютин Валерий Павлович',
    birthDay: '09101948',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Коровников Владимир Гергиевич',
    birthDay: '07111935',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Корчажкин Федор Васильевич',
    birthDay: '10111950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Наянзов Сергей Викторович',
    birthDay: '03081961',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Чубатюк Владимир Данилович',
    birthDay: '24021954',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Миндияров Ганиятулла Зиннатович',
    birthDay: '04081957',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Медведев Александр Иванович',
    birthDay: '07071958',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Куралесов Валерий Петрович',
    birthDay: '21051953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Мохирев Александр Геннадьевич',
    birthDay: '29031957',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Пигин Михаил Николаевич',
    birthDay: '01011952',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Сивков Анатолий Владимирович',
    birthDay: '03111934',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Неп Владимир Андреевич',
    birthDay: '03111955',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Афанасьев Анатолий Кузьмич',
    birthDay: '29071936',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Вовчук Александр Иванович',
    birthDay: '25121967',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Азаренков Владимир Никифорович',
    birthDay: '16081957',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Сулейман Владимир Леонидович',
    birthDay: '18031953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Хаперский Василий Семенович',
    birthDay: '23041945',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Огородников Григорий Александрович',
    birthDay: '15011940',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Кондратьев Василий Дмитриевич',
    birthDay: '26031953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Лобанов Валерий Васильевич',
    birthDay: '19021954',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Плеханов Андрей Константинович',
    birthDay: '17011965',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Агафонов Николай Николаевич',
    birthDay: '23041950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Смагин Вячеслав Николаевич',
    birthDay: '02111938',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Мухарамов Ринат Минирович',
    birthDay: '09051961',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Кочергин Анатолий Иванович',
    birthDay: '12071958',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Гатиятулин Гариф Рамазанович',
    birthDay: '11041950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Воронин Александ Павлович',
    birthDay: '03051960',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Коробко Александр Григорьевич',
    birthDay: '07081958',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Бахарев Валерий Петрович',
    birthDay: '12091950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Абрамов Николай Иванович',
    birthDay: '22111940',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Курбаков Виктор Александрович',
    birthDay: '13101953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Галеев Фарит Валеевич',
    birthDay: '08101939',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Кичеев Андрей Николаевич',
    birthDay: '07091962',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Новосельцев Иван Федорович',
    birthDay: '14101949',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Рамазанов Мавлит Давлитович',
    birthDay: '05101954',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Верховых Иван Семенович',
    birthDay: '28011951',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Полыганов Иван Борисович',
    birthDay: '15011962',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Мозелов Зиновий Николаевич',
    birthDay: '23091939',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Лезин Михаил Михайлович',
    birthDay: '30111961',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Власов Тимофей Петрович',
    birthDay: '25061950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Самойлов Михаил Васильевич',
    birthDay: '17031947',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Родыгин Николай Георгиевич',
    birthDay: '29061949',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Зеленокор Александр Иванович',
    birthDay: '27071957',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Скригаловский Виктор Игнатьевич',
    birthDay: '04111954',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Иванов Александр Федорович',
    birthDay: '29071946',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Рубан Виталий Леонидович',
    birthDay: '06101950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Кушмин Николай Михайлович',
    birthDay: '08101948',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Хафизов Тагир Нурулливич',
    birthDay: '02051950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Пьянков Анатолий Семенович',
    birthDay: '27061940',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Шабурников Николай Григорьевич',
    birthDay: '04061949',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Бурдейный Василий Пахомович',
    birthDay: '11091939',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Симайкин Сергей Николаевич',
    birthDay: '29041956',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Гафуров Рафаэль Абдрахманович',
    birthDay: '22041955',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Файзулин Уразай Зуфарович',
    birthDay: '25041944',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Сырников Юрий Тихонович',
    birthDay: '01061949',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Журавлев Евгений Александрович',
    birthDay: '08111947',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Шестаков Владимир Африканович',
    birthDay: '20091961',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Лаврик Николай Петрович',
    birthDay: '24091954',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Савушкин Владимир Викторович',
    birthDay: '20051953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Мальцев Владимир Юрьевич',
    birthDay: '17121955',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Савинов Петр Алексеевич',
    birthDay: '28111954',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Плотников Яков Игнатьевич',
    birthDay: '11081947',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Ананьин Александр Петрович',
    birthDay: '16121947',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Медведев Георгий Иванович',
    birthDay: '11091944',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Кудрявцев Анатолий Филиппович',
    birthDay: '07071954',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Журавлев Александр Михайлович',
    birthDay: '13011958',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Тур Владимир Анатольевич',
    birthDay: '04061952',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Вишняков Валерий Семенович',
    birthDay: '02071941',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Вишняков Василий Васильевич',
    birthDay: '20081950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Ярушников Владимир Семенович',
    birthDay: '27021949',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Третьяков Владимир Киррилович',
    birthDay: '15071960',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Юдин Иван Николаевич',
    birthDay: '14091948',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Шамин Сергей Николаевич',
    birthDay: '16061948',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Шлепнев Александр Михайлович',
    birthDay: '11061955',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Нистратов Сергей Рубинович',
    birthDay: '02061955',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Никишев Владимир Андреевич',
    birthDay: '02041951',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Козлов Виктор Григорьевич',
    birthDay: '21041956',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Бодякин Борис Михайлович',
    birthDay: '25061949',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Шакуров Павел Федорович',
    birthDay: '12061949',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Маланов Виталий Васильевич',
    birthDay: '25011930',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Ивлев Ирий Павлович',
    birthDay: '23061952',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Бородин Лев Николаевич',
    birthDay: '13081950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Сенников Леонид Николаевич',
    birthDay: '11021953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Кривой Николай Васильевич',
    birthDay: '26051960',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Шарафутдинов Гафият Зайнулович',
    birthDay: '10071939',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Колесников Александр Юрьевич',
    birthDay: '22061958',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Деев Владимир Викторович',
    birthDay: '02111939',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Селянин Иван Иванович',
    birthDay: '28091947',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Ширшиков Валерий Викторович',
    birthDay: '05111961',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Рева Игорь Александрович',
    birthDay: '31031959',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Радченко Геннадий Иванович',
    birthDay: '16061940',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Давлятов Рифкат Галямутдинович',
    birthDay: '05111936',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Зайцев Владимир Федорович',
    birthDay: '28071948',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Кузовенков Николай Викторович',
    birthDay: '24111964',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Валеев Марван Миннигареевич',
    birthDay: '05091952',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Мухарамов Рамиль Мирасович',
    birthDay: '15011952',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Чистов Валентин Сергеевич',
    birthDay: '23031949',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Козешкурт Сергей Афанасьевич',
    birthDay: '08091953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Махмутов Равиль Хужаевич',
    birthDay: '16081938',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Панов Юрий Васильевич',
    birthDay: '14121951',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Слекишин Иван Александрович',
    birthDay: '06051964',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Шаймухаметов Арслан Шмяжевич',
    birthDay: '07091952',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Мохонько Александр Иванович',
    birthDay: '14071959',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Вельчев Иван Дмитриевич',
    birthDay: '19011950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Дышкант Михаил Васильевич',
    birthDay: '15071942',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Мышкин Вениамин Прокопьевич',
    birthDay: '15101939',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Гусев Валерий Владимирович',
    birthDay: '01011962',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Исаев Юрий Федорович',
    birthDay: '31051965',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Колесников Борис Михайлович',
    birthDay: '29031946',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Виноградов Сергей Васильевич',
    birthDay: '26101959',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Баранцев Олег Павлович',
    birthDay: '05041959',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Мельников Юрий Николаевич',
    birthDay: '04081953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Веселов Владимир Павлович',
    birthDay: '24101948',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Калинин Виктор Александрович',
    birthDay: '04111953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Поляков Валерий Мартемьянович',
    birthDay: '14111945',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Павлухин Александр Иванович',
    birthDay: '08081943',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Боровлев Владимир Ильич',
    birthDay: '16051943',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Евдокимов Владимир Андреевич',
    birthDay: '10051946',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Гурентьев Анатолий Александрович',
    birthDay: '10041946',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Тюнин Иван Трофимович',
    birthDay: '28011932',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Ладейщиков Сергей Петрович',
    birthDay: '24121960',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Семенов Михаил Иванович',
    birthDay: '03071956',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Афонин Юрий Степанович',
    birthDay: '07031943',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Юртеев Владимир Александрович',
    birthDay: '20031948',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Самичев Александр Михайлович',
    birthDay: '31031958',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Карамашев Александр Анатольевич',
    birthDay: '03061957',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Савельев Владимир Борисович',
    birthDay: '28111959',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Оленченко Петр Яковлевич',
    birthDay: '16091953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Шумилин Валентин Васильевич',
    birthDay: '02011957',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Сысоев Владимир Николаевич',
    birthDay: '22091959',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Варганов Василий Иванович',
    birthDay: '07121955',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'СЕЛЯНИН Тимофей Николаевич',
    birthDay: '26021955',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Садыков Нагим Файзрахманович',
    birthDay: '01041951',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Ильин Николай Павлович',
    birthDay: '23051950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Серебрянников Виктор Александрович',
    birthDay: '19041957',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Митюшкин Виктор Егорович',
    birthDay: '12091931',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Двойнов Алексей Николаевич',
    birthDay: '13031957',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Матвеев Анатолий Петрович',
    birthDay: '25011962',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Кокшаров Андрей Павлович',
    birthDay: '31051964',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Мусалимов Ахат Аюпович',
    birthDay: '01081934',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Режапов Денислам Шаихович',
    birthDay: '16121938',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Черепанов Александр Павлович',
    birthDay: '22091938',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Пилицын Николай Иванович',
    birthDay: '03051962',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Саримом Рамиль Махмутович',
    birthDay: '04011935',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Колчин Федор Михайлович',
    birthDay: '13021931',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Семенищев Николай Васильевич',
    birthDay: '02011949',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Бобылев Иван Петрович',
    birthDay: '07061936',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Шакиров Тимергазиян Билалович',
    birthDay: '03081948',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Ибрагимов Рефмир Мирзоевич',
    birthDay: '22111951',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Даниленко Василий Васильевич',
    birthDay: '15021957',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Авоян Спартак Мясникович',
    birthDay: '03111964',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Истомин Николай Николаевич',
    birthDay: '23091949',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Мингажин Кираматула Хасанович',
    birthDay: '15091933',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Горбань Василий Иванович',
    birthDay: '19111953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Артюхов Александр Степанович',
    birthDay: '10081946',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Гартман Владимир Кондратьевич',
    birthDay: '01011953',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Юсупов Марат Ваккасович',
    birthDay: '23071939',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Кудрин Евгений Николаевич',
    birthDay: '10121950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Абрамов Александр Владимирович',
    birthDay: '14101958',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Попков Александр Петрович',
    birthDay: '05041950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Белоусов Геннадий Петрович',
    birthDay: '21121939',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Харченко Анатолий Алексеевич',
    birthDay: '07021950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Щелконогов Василий Анатольевич',
    birthDay: '20041951',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  },
  {
    name: 'Бигнов Ришат Мушарифович',
    birthDay: '21021950',
    userId: '5c0dabf4514d854867aec7ed',
    departmentId: '5c0daaf7d8a39a482a2254f1'
  }
]